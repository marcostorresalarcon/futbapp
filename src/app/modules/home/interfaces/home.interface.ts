export interface IDataApi {
    service: string
    data: any;
    type: string
}

export class ILeagues{
    "id": string
    "Nombre De La Liga": string = ''
    "Identificador": string = ''
    "Logo de la Liga": string = ''
}

export class ITeams {
    "Liga": string = ''
    "Logo del Equipo": string = ''
    "Nombre del equipo": string = ''
    "id": string = ''
}

export class IPlayers {
    "Avatar": string = ''
    "Nombre del Jugador": string = ''
    "id": string = ''
    "teamId": string = ''
}