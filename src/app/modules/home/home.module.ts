import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { SlideModule } from './components/slide/slide.module';
import { NavModule } from './components/nav/nav.module';

@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SlideModule,
    NavModule,    
  ]
})
export class HomeModule { }
