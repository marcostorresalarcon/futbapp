import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { IDataApi } from '../interfaces/home.interface';
import { ILeagues, IPlayers, ITeams } from 'src/app/modules/home/interfaces/home.interface';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(
    private api: ApiService
  ) { }

  getAllLeagues():any {
    const data: IDataApi = {
      service: 'leagues',
      type: 'get',
      data: null
    }
    return this.api.api(data)
  }

  getAllTeams():any {
    const data: IDataApi = {
      service: 'teams',
      type: 'get',
      data: null
    }
    return this.api.api(data)
  }

  getAllPlayers():any {
    const data: IDataApi = {
      service: 'players',
      type: 'get',
      data: null
    }
    return this.api.api(data)
  }

  add(type:string, item: ILeagues | ITeams | IPlayers | any):any{
    const data: IDataApi = {
      service: type,
      type: 'post',
      data: item
    }
    return this.api.api(data)
  }

  edit(type: string, item: ILeagues | ITeams | IPlayers | any):any {
    const data: IDataApi = {
      service: `${type}/${item.id}`,
      type: `patch`,
      data: item
    }
    return this.api.api(data)
  }

  delete(type: string, item: ILeagues | ITeams | IPlayers | any):any {
    const data: IDataApi = {
      service: `${type}/${item.id}`,
      type: `delete`,
      data: item
    }
    return this.api.api(data)
  }

}
