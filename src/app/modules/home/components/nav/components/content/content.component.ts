import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { ILeagues, IPlayers, ITeams } from 'src/app/modules/home/interfaces/home.interface';
import { CCurrentContent } from 'src/app/modules/home/models/home';
import { HomeService } from 'src/app/modules/home/services/home.service';
import { GeneralService } from './../../../../../../services/general.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit, OnChanges {

  @Input() content: CCurrentContent = new CCurrentContent
  @Output() eventsContent = new EventEmitter<any>();
  dateTmp: CCurrentContent = this.content
  currentItem: ILeagues | ITeams | IPlayers
  isVisible: boolean = false
  editItem: boolean = false
  form: ILeagues | ITeams | IPlayers
  leagues: ILeagues[]
  teams: ITeams []

  constructor(
    private generalService: GeneralService,
    private homeService: HomeService
  ) {
    this.getAllLeagues()
    this.getAllTeams()
  }
  
  ngOnInit(): void {
  }

  ngOnChanges(){
    this.content = { ...this.content}
    this.dateTmp = { ...this.content }    
    this.assignForm()
  }

  assignForm(){
    switch (this.content.type) {
      case 'Ligas':
        this.form = new ILeagues
        break;
      case 'Equipos':
        this.form = new ITeams
        break;
      case 'Jugadores':
        this.form = new IPlayers
        break;
    }
  }

  search($event:any){
    const key = $event.target.value
    let keyData = ''
    switch (this.dateTmp.type) {
      case 'Ligas':
        keyData = 'Nombre De La Liga'
        break;
      case 'Equipos':
        keyData = 'Nombre del equipo'
        break;
      case 'Jugadores':
        keyData = 'Nombre del Jugador'
        break;
    }
    if(key){
      this.content.data = [...this.dateTmp.data ]
      this.content.data = this.content.data.filter((data:any)=>{      
        if (data[keyData].indexOf(key) > -1){
          return data
        }
      })
    }else{
      this.content.data = [...this.dateTmp.data ]
    }
  }

  openView(item:ILeagues|ITeams|IPlayers){
    this.currentItem = item 
  }

  showModal(): void {
    this.isVisible = true;
  }

  getServiceName():string{
    switch (this.content.type) {
      case 'Ligas':
        return 'leagues'
      case 'Equipos':
        return 'teams'
      case 'Jugadores':
        return 'players'
      default: 
      return ''    
    }
  }

  handleOk(): void {
    this.eventsContent.emit({
      type: this.editItem ? 'edit': 'add',
      data: this.form,
      typeItem: this.getServiceName()
    })
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  addEdit(isEdit: boolean){
    if (isEdit){
      this.editItem = isEdit
      this.form = this.currentItem
    }    
    this.isVisible = true
  }

  delete(){
    this.eventsContent.emit({
      type: 'delete',
      data: this.currentItem,
      typeItem: this.getServiceName()
    })
    this.isVisible = false;
  }

  getAllLeagues() {
    this.homeService.getAllLeagues().subscribe((leagues: ILeagues[]) => {
      this.leagues = leagues
      this.generalService.c('getAllLeagues', leagues)
    }, (error: any) => {
      this.generalService.createMessage('Error', error.error.message)
    })
  }
  getAllTeams() {
    this.homeService.getAllTeams().subscribe((teams: ITeams[]) => {
      this.teams = teams
      this.generalService.c('getAllLeagues', teams)
    }, (error: any) => {
      this.generalService.createMessage('Error', error.error.message)
    })
  }
 


}
