import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './nav.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { ContentComponent } from './components/content/content.component';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzListModule } from 'ng-zorro-antd/list';
import { FormsModule } from '@angular/forms';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';

@NgModule({
  declarations: [
    NavComponent,
    ContentComponent
  ],
  imports: [  
    CommonModule,
    NzIconModule,
    NzSpinModule,
    NzListModule,
    FormsModule,
    NzModalModule,
    NzPopconfirmModule
  ],
  exports: [
    NavComponent
  ]
})
export class NavModule { }
