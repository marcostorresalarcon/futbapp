import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/services/general.service';
import { ILeagues, ITeams, IPlayers } from '../../interfaces/home.interface';
import { CCurrentContent } from '../../models/home';
import { HomeService } from '../../services/home.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  tabs = ['Ligas','Equipos','Jugadores']
  isVisibleContent: boolean = false
  currentContent: CCurrentContent = new CCurrentContent;
  isLoad: boolean = true

  constructor(
      private homeService: HomeService,
      private generalService: GeneralService
    ) {
     }

  ngOnInit(): void {
  }

  openInfo(tab:string){
    this.isLoad = true    
    switch (tab) {
      case 'Ligas':
        this.getAllLeagues()
        this.currentContent.type = tab
        break;
      case 'Equipos':
        this.getAllTeams()
        this.currentContent.type = tab
        break;
      case 'Jugadores':
        this.getAllPlayers()
        this.currentContent.type = tab
        break;
      case 'leagues':
        this.getAllLeagues()
        break;
      case 'teams':
        this.getAllTeams()
        break;
      case 'players':
        this.getAllPlayers()
        break;
    
      default:
        this.generalService.createMessage('Error', 'No se pudo encontrar el tipo')
        break;
    }
    this.isVisibleContent = true
  }

  closeInfo(){
    this.isVisibleContent = false
  }

  getAllLeagues(){
    this.homeService.getAllLeagues().subscribe((leagues: ILeagues[]) => {
      this.currentContent.data = leagues
      this.isLoad = false
      this.generalService.c('getAllLeagues', leagues)
    }, (error:any) => {
      this.generalService.createMessage('Error', error.error.message)
    })
  }
  getAllTeams(){
    this.homeService.getAllTeams().subscribe((teams: ITeams[]) => {
      this.currentContent.data = teams
      this.isLoad = false
      this.generalService.c('getAllLeagues', teams)
    }, (error: any) => {
      this.generalService.createMessage('Error', error.error.message)
    })
  }
  getAllPlayers(){
    this.homeService.getAllPlayers().subscribe((players: IPlayers[]) => {
      this.currentContent.data = players
      this.isLoad = false
      this.generalService.c('getAllLeagues', players)
    }, (error: any) => {
      this.generalService.createMessage('Error', error.error.message)
    })
  }

  eventsContent($event){
    this.generalService.c('eventsContent', $event)

    switch ($event.type) {
      case 'add':
        const ramdom = this.generalService.getRandomString(10)
        $event.data['Identificador'] = ramdom
        $event.data['id'] = ramdom
        this.homeService.add($event.typeItem, $event.data).subscribe(response=>{
          this.generalService.c('Response add', response)
          this.openInfo($event.typeItem)
        },error=>{
          this.generalService.c('Error', error)
          this.generalService.createMessage('Error', error.error.message)
        })
        break;
      case 'edit':        
        this.homeService.edit($event.typeItem, $event.data).subscribe(response => {
          this.generalService.c('Response add', response)
          this.openInfo($event.typeItem)
        },error=>{
          this.generalService.c('Error', error)
          this.generalService.createMessage('Error', error.error.message)
        })
        break;
      case 'delete':        
        this.homeService.delete($event.typeItem, $event.data).subscribe(response => {
          this.generalService.c('Response add', response)
          this.openInfo($event.typeItem)
        },error=>{
          this.generalService.c('Error', error)
          this.generalService.createMessage('Error', error.error.message)
        })
        break;
    
      default:
        break;
    }


  }

}
