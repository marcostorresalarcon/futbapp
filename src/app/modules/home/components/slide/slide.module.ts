import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlideComponent } from './slide.component';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';


@NgModule({
  declarations: [    
    SlideComponent
  ],
  imports: [
    CommonModule,    
    NzCarouselModule
  ],
  exports: [
    SlideComponent
  ]
})
export class SlideModule { }
