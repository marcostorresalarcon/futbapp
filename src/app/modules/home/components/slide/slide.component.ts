import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.scss']
})
export class SlideComponent implements OnInit {

  slides:string[] = []

  constructor() {
    this.getSlides()
  }

  ngOnInit(): void {
  }

  getSlides(){
    for (let i = 3; i > 0; i--) {
      this.slides = [...this.slides, `/assets/img/slides/${i}.jpg`]      
    }
  }

}
